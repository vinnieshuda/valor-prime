// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "InteractableWidgetInterface.h"
#include "PickupableWeapon.h"

APickupableWeapon::APickupableWeapon()
{
	bReplicates = true;
	bReplicateMovement = true;

	ItemMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Pickupable Mesh");
	ItemMeshComponent->SetIsReplicated(true);
	SetRootComponent(ItemMeshComponent);

	ItemMeshComponent->SetSimulatePhysics(true);

	InteractableComponent->SetupAttachment(ItemMeshComponent);
	InteractableComponent->SetIsReplicated(true);

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>("Widget Component");
	WidgetComponent->ComponentTags.Add(TEXT("InteractWidget"));

	InteractableComponent->WidgetComponentTag = TEXT("InteractWidget");
}

void APickupableWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickupableWeapon, ItemInventoryData);
}

void APickupableWeapon::SetupPickupableWeapon(int32 inCurrentAmmo, int32 inCurrentAmmoinClip, FInventoryData inItemInventoryData, UStaticMesh* inStaticMesh)
{
	ItemInventoryData = inItemInventoryData;

	CurrentAmmo = inCurrentAmmo;
	CurrentAmmoInClip = inCurrentAmmoinClip;

	if (inStaticMesh)
	{
		ItemMeshComponent->SetStaticMesh(inStaticMesh);

		Client_SetupCollisionBox(inStaticMesh->GetBoundingBox().GetExtent());

		InteractableComponent->SetBoxExtent(inStaticMesh->GetBoundingBox().GetExtent());
	}
}

void APickupableWeapon::Client_SetupCollisionBox_Implementation(FVector inBoxExtent)
{
	InteractableComponent->SetBoxExtent(inBoxExtent);
}

void APickupableWeapon::OnInteractPressed_Implementation(AActor* InteractOwner)
{
	if (IsPendingKillOrUnreachable())
		return;

	Super::OnInteractPressed_Implementation(InteractOwner);

	if (!HasAuthority())
		return;

	AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(InteractOwner);

	if (!ShooterCharacter)
		return;

	FInventoryDatabase* ItemDB = ItemInventoryData.ItemRowHandler.DataTable->FindRow<FInventoryDatabase>(ItemInventoryData.ItemRowHandler.RowName, TEXT("Lookup"));

	if (!ItemDB)
		return;

	ShooterCharacter->SpawnAndAddWeapon(ItemDB->WeaponClass);

	Destroy();
}



