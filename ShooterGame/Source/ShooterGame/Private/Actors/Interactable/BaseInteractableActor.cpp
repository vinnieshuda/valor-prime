// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Interfaces/Interactable/InteractableWidgetInterface.h"
#include "BaseInteractableActor.h"


// Sets default values
ABaseInteractableActor::ABaseInteractableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	RootScene = CreateDefaultSubobject<USceneComponent>("Scene Root");
	InteractableComponent = CreateDefaultSubobject<UInteractableObjectHelper>("Interactable Component");

	SetRootComponent(RootScene);
	InteractableComponent->SetRootComponentAttachment(RootScene);

}

// Called when the game starts or when spawned
void ABaseInteractableActor::BeginPlay()
{
	Super::BeginPlay();

	/*if (!HasAuthority())
		InteractableComponent->GetUserWidgetObject()->SetVisibility(ESlateVisibility::Hidden);*/

	
}

// Called every frame
void ABaseInteractableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseInteractableActor::OnInteractableEntered_Implementation()
{

}

void ABaseInteractableActor::OnInteractableLeaved_Implementation()
{

}

void ABaseInteractableActor::OnInteractableTargeted_Implementation(AActor* InteractOwner)
{
	if (InteractableComponent->GetControlledWidgetComponent())
	{
		InteractableComponent->GetControlledWidgetComponent()->SetVisibility(true);

		//bool bOk = InteractableComponent->GetControlledWidgetComponent()->GetUserWidgetObject()->Implements<IInteractableWidgetInterface>();
		IInteractableWidgetInterface* IF = Cast<IInteractableWidgetInterface>(InteractableComponent->GetControlledWidgetComponent()->GetUserWidgetObject());

		if (IF)
			IF->Execute_OnInteractableActorTargeted(InteractableComponent->GetControlledWidgetComponent()->GetUserWidgetObject(), InteractOwner);

		if(InteractableComponent->GetControlledWidgetComponent()->GetUserWidgetObject()->Implements<UInteractableWidgetInterface>())
			IInteractableWidgetInterface::Execute_OnInteractableActorTargeted(InteractableComponent->GetControlledWidgetComponent()->GetUserWidgetObject(), InteractOwner);
	}
}

void ABaseInteractableActor::OnInteractableWasTargeted_Implementation()
{
	if (InteractableComponent->GetControlledWidgetComponent())
		InteractableComponent->GetControlledWidgetComponent()->SetVisibility(false);
}

void ABaseInteractableActor::OnInteractPressed_Implementation(AActor* InteractOwner)
{

}

