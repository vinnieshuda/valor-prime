// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "InteractableObjectHelper.h"

UInteractableObjectHelper::UInteractableObjectHelper()
{
	PrimaryComponentTick.bCanEverTick = false;

	SetCollisionResponseToAllChannels(ECR_Ignore);
	bGenerateOverlapEvents = true;
	SetCanEverAffectNavigation(false);

}

void UInteractableObjectHelper::SetRootComponentAttachment(USceneComponent* InParent, FName InSocketName /* = NAME_None */)
{
	SetupAttachment(InParent, InSocketName);
	/*CollisionBox->SetupAttachment(InParent, InSocketName);*/
}


#if WITH_EDITOR
void UInteractableObjectHelper::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	SetCollisionResponseToAllChannels(ECR_Ignore);
	SetCollisionResponseToChannel(InteractableCollisionChannel, ECR_Overlap);
	SetCollisionObjectType(InteractableCollisionChannel);

	if (!WidgetComponentClass)
		return;

	TArray<UActorComponent*> WidgetComponents = GetOwner()->GetComponentsByTag(WidgetComponentClass, WidgetComponentTag);

	if (WidgetComponents.Num() > 0)
	{
		WidgetComponent = Cast<UWidgetComponent>(WidgetComponents[0]);
		WidgetComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

}
#endif

void UInteractableObjectHelper::OnComponentCreated()
{
	Super::OnComponentCreated();

	TRACE("Component Created")
}

void UInteractableObjectHelper::PostInitProperties()
{
	Super::PostInitProperties();

	TRACE("Post Init Properties")
}

void UInteractableObjectHelper::BeginPlay()
{
	Super::BeginPlay();

	if (GetControlledWidgetComponent())
	{
		GetControlledWidgetComponent()->SetVisibility(false);
	}
}

UWidgetComponent* UInteractableObjectHelper::GetControlledWidgetComponent()
{
	if (WidgetComponent)
		return WidgetComponent;

	if (!WidgetComponentClass)
		return nullptr;

	TArray<UActorComponent*> WidgetComponents = GetOwner()->GetComponentsByTag(WidgetComponentClass, WidgetComponentTag);

	if (WidgetComponents.Num() > 0)
	{
		WidgetComponent = Cast<UWidgetComponent>(WidgetComponents[0]);
	}else return nullptr;

	return WidgetComponent;
}

