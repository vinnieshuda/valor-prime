// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Interfaces/Interactable/InteractableInterface.h"
#include "InteractableSearchComponent.h"

UInteractableSearchComponent::UInteractableSearchComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetCollisionResponseToAllChannels(ECR_Ignore);
	bGenerateOverlapEvents = true;

	SetIsReplicated(true);

	OnComponentBeginOverlap.AddDynamic(this, &UInteractableSearchComponent::OnInteractableActorOverlapBegin);
	OnComponentEndOverlap.AddDynamic(this, &UInteractableSearchComponent::OnInteractableActorOverlapEnd);

	SetCanEverAffectNavigation(false);

	bUseCameraForTrace = true;
}

void UInteractableSearchComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*if (!bInteractSearchStarted && bUseSphereSearch)
		return;*/

	if (!GetWorld())
		return;

	if (!GetOwner())
		return;

	APawn* OwnerPawn = Cast<APawn>(GetOwner());

	if (!OwnerPawn)
		return;

	APlayerController* OwnerPC = Cast<APlayerController>(OwnerPawn->GetController());

	if (!OwnerPC)
		return;

	// Perform interaction Search Locally we do not need to test this on server right now
	// additional check will happens when player press interact to avoid hacking
	if (!OwnerPC->IsLocalController())
		return;

	FHitResult TraceHitResult;
	FCollisionObjectQueryParams TraceObjectQueryParams;
	FCollisionQueryParams TraceQueryParams;

	TraceQueryParams.AddIgnoredActor(GetOwner());
	
	FVector TraceStartLocation;
	FVector TraceEndLocation;

	if (bUseCameraForTrace)
	{
		TraceStartLocation = OwnerPC->PlayerCameraManager->GetCameraLocation();
		TraceEndLocation = TraceStartLocation + (OwnerPC->GetActorForwardVector() * MaxInteractableDistance);
	}

	for (TEnumAsByte<ECollisionChannel> CChannel : BlockingChannels)
	{
		TraceObjectQueryParams.AddObjectTypesToQuery(CChannel);
	}

	TraceObjectQueryParams.AddObjectTypesToQuery(InteractableCollisionChannel);

	bool bHitSomething = GetWorld()->LineTraceSingleByObjectType(TraceHitResult, TraceStartLocation, TraceEndLocation, TraceObjectQueryParams, TraceQueryParams);

	if (bHitSomething)
	{
		if (bDebugDraw)
		{
			DrawDebugLine(GetWorld(), TraceStartLocation, TraceHitResult.Location, FColor::Red, true, 10.f, 1.f, 10.f);
			DrawDebugPoint(GetWorld(), TraceHitResult.Location, 15.f, FColor::Green, true, 10.f);
		}	

		AActor* HitActor = TraceHitResult.GetActor();

		// Just for Safe
		if (!HitActor && PreviousHitActor)
		{
			IInteractableInterface* InteractableIF = Cast<IInteractableInterface>(PreviousHitActor);

			if(InteractableIF)
				InteractableIF->Execute_OnInteractableWasTargeted(PreviousHitActor);

			PreviousHitActor = nullptr;

		}

		if (!HitActor)
			return;

		IInteractableInterface* HitActorInterface = Cast<IInteractableInterface>(HitActor);

		// Check we have prev hit but was hit non interactable object
		if (!HitActorInterface && PreviousHitActor)
		{
			IInteractableInterface* InteractablePrevIF = Cast<IInteractableInterface>(PreviousHitActor);

			if (InteractablePrevIF)
				InteractablePrevIF->Execute_OnInteractableWasTargeted(PreviousHitActor);

			PreviousHitActor = nullptr;

			return;
		}

		// No Interface implemented return
		if (!HitActorInterface)
			return;

		// If we same actor was hit return
		if (PreviousHitActor == HitActor)
			return;

		// Check if we hit different actor with interface
		if (HitActor != PreviousHitActor && PreviousHitActor)
		{
			IInteractableInterface* InteractablePrevIF = Cast<IInteractableInterface>(PreviousHitActor);

			if (InteractablePrevIF)
				InteractablePrevIF->Execute_OnInteractableWasTargeted(PreviousHitActor);
		}

		//At this point our actor is valid and interactable

		PreviousHitActor = HitActor;

		HitActorInterface->Execute_OnInteractableTargeted(HitActor, GetOwner());

		return;
	}

	if (PreviousHitActor)
	{
		IInteractableInterface* InteractablePrevIF = Cast<IInteractableInterface>(PreviousHitActor);

		if (InteractablePrevIF)
			InteractablePrevIF->Execute_OnInteractableWasTargeted(PreviousHitActor);

		PreviousHitActor = nullptr;
	}
		
	if(bDebugDraw)
		DrawDebugLine(GetWorld(), TraceStartLocation, TraceEndLocation, FColor::Red, true, 10.f, 1.f, 10.f);

}

void UInteractableSearchComponent::OnComponentCreated()
{
	Super::OnComponentCreated();
}

#ifdef WITH_EDITOR

void UInteractableSearchComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	SetSphereRadius(InteractableSearchRange);

	SetCollisionResponseToAllChannels(ECR_Ignore);
	SetCollisionResponseToChannel(InteractableCollisionChannel, ECR_Overlap);
	SetCollisionObjectType(InteractableCollisionChannel);
}

#endif

void UInteractableSearchComponent::OnInteractableActorOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	TRACE("Interactable Actor Overlap Begin")
}

void UInteractableSearchComponent::OnInteractableActorOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	TRACE("Interactable Actor Overlap End")
}

void UInteractableSearchComponent::OnInteractPressed(AActor* InteractOwner)
{
	if (!PreviousHitActor)
		return;

	// Perform fast distance check to avoid hacking
	float ObjectDistance = (PreviousHitActor->GetActorLocation() - GetOwner()->GetActorLocation()).Size();

	if (ObjectDistance > MaxInteractableDistance)
		return;

	IInteractableInterface* InteractablePrevIF = Cast<IInteractableInterface>(PreviousHitActor);

	if (InteractablePrevIF)
		InteractablePrevIF->Execute_OnInteractPressed(PreviousHitActor, GetOwner());

	if (!GetOwner()->HasAuthority())
		Server_OnInteractPressed(InteractOwner, PreviousHitActor);
}

void UInteractableSearchComponent::Internal_OnInteractPressed(AActor* InteractOwner, AActor* InteractedActor)
{
	if (!InteractedActor)
		return;

	// Perform fast distance check to avoid hacking
	float ObjectDistance = (InteractedActor->GetActorLocation() - GetOwner()->GetActorLocation()).Size();

	if (ObjectDistance > MaxInteractableDistance)
		return;

	IInteractableInterface* InteractablePrevIF = Cast<IInteractableInterface>(InteractedActor);

	if (InteractablePrevIF)
		InteractablePrevIF->Execute_OnInteractPressed(InteractedActor, GetOwner());

}

void UInteractableSearchComponent::Server_OnInteractPressed_Implementation(AActor* InteractOwner, AActor* InteractedActor)
{
	Internal_OnInteractPressed(InteractOwner, InteractedActor);
}

bool UInteractableSearchComponent::Server_OnInteractPressed_Validate(AActor* InteractOwner, AActor* InteractedActor)
{
	return true;
}