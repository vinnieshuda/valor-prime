// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "InteractableWidgetInterface.h"


// Add default functionality here for any IInteractableWidgetInterface functions that are not pure virtual.

void IInteractableWidgetInterface::OnInteractableActorEntered_Implementation()
{

}

void IInteractableWidgetInterface::OnInteractableActorLeaved_Implementation()
{

}

void IInteractableWidgetInterface::OnInteractableActorTargeted_Implementation(AActor* InteractOwner)
{

}

void IInteractableWidgetInterface::OnInteractableActorNotTargeted_Implementation()
{

}

void IInteractableWidgetInterface::OnInteractPressed_Implementation()
{

}

void IInteractableWidgetInterface::OnInteractTimeUpdated_Implementation(float inElapsedTime)
{

}