// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "InteractableInterface.h"


// Add default functionality here for any IInteractableInterface functions that are not pure virtual.

void IInteractableInterface::OnInteractableEntered_Implementation()
{

}

void IInteractableInterface::OnInteractableLeaved_Implementation()
{

}

void IInteractableInterface::OnInteractableTargeted_Implementation(AActor* InteractOwner)
{

}

void IInteractableInterface::OnInteractableWasTargeted_Implementation()
{

}

void IInteractableInterface::OnInteractPressed_Implementation(AActor* InteractOwner)
{

}
