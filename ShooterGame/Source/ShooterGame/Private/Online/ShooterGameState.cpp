// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Weapons/ShooterWeapon.h"
#include "Actors/Interactable/PickupableWeapon.h"
#include "Online/ShooterPlayerState.h"
#include "ShooterGameInstance.h"

AShooterGameState::AShooterGameState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NumTeams = 0;
	RemainingTime = 0;
	bTimerPaused = false;
}

void AShooterGameState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( AShooterGameState, NumTeams );
	DOREPLIFETIME( AShooterGameState, RemainingTime );
	DOREPLIFETIME( AShooterGameState, bTimerPaused );
	DOREPLIFETIME( AShooterGameState, TeamScores );
}

void AShooterGameState::SpawnWeaponToWorld(AActor* InstigatorActor, int32 inCurrentAmmo, int32 inCurrentAmmoInClip, FInventoryData inInventoryData)
{
	if (!inInventoryData.ItemRowHandler.DataTable)
		return;

	if (inInventoryData.ItemRowHandler.IsNull())
		return;

	// Create Spawn location
	FVector Spawnlocation = InstigatorActor->GetActorLocation();
	FQuat NewRotation;

	FTransform SpawnTransform;

	SpawnTransform.SetRotation(NewRotation);

	Spawnlocation.Z += 50.f;
	Spawnlocation.X += FMath::RandRange(-100, 100);
	Spawnlocation.Y += FMath::RandRange(-100, 100);

	SpawnTransform.SetLocation(Spawnlocation);

	if (PickupableWeaponClass)
	{
		FInventoryDatabase* ItemData = inInventoryData.ItemRowHandler.DataTable->FindRow<FInventoryDatabase>(inInventoryData.ItemRowHandler.RowName, TEXT("Inventory Lookup"));

		if (!ItemData)
			return;

		APickupableWeapon* WeaponActor = GetWorld()->SpawnActorDeferred<APickupableWeapon>(PickupableWeaponClass, SpawnTransform, this, nullptr, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
		WeaponActor->SetupPickupableWeapon(inCurrentAmmo, inCurrentAmmoInClip, inInventoryData, ItemData->StaticMesh);

		if (WeaponActor)
			UGameplayStatics::FinishSpawningActor(WeaponActor, FTransform(NewRotation, Spawnlocation));
		else WeaponActor->Destroy();
	}
}

void AShooterGameState::GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const
{
	OutRankedMap.Empty();

	//first, we need to go over all the PlayerStates, grab their score, and rank them
	TMultiMap<int32, AShooterPlayerState*> SortedMap;
	for(int32 i = 0; i < PlayerArray.Num(); ++i)
	{
		int32 Score = 0;
		AShooterPlayerState* CurPlayerState = Cast<AShooterPlayerState>(PlayerArray[i]);
		if (CurPlayerState && (CurPlayerState->GetTeamNum() == TeamIndex))
		{
			SortedMap.Add(FMath::TruncToInt(CurPlayerState->Score), CurPlayerState);
		}
	}

	//sort by the keys
	SortedMap.KeySort(TGreater<int32>());

	//now, add them back to the ranked map
	OutRankedMap.Empty();

	int32 Rank = 0;
	for(TMultiMap<int32, AShooterPlayerState*>::TIterator It(SortedMap); It; ++It)
	{
		OutRankedMap.Add(Rank++, It.Value());
	}
	
}


void AShooterGameState::RequestFinishAndExitToMainMenu()
{
	if (AuthorityGameMode)
	{
		// we are server, tell the gamemode
		AShooterGameMode* const GameMode = Cast<AShooterGameMode>(AuthorityGameMode);
		if (GameMode)
		{
			GameMode->RequestFinishAndExitToMainMenu();
		}
	}
	else
	{
		// we are client, handle our own business
		UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
		if (GameInstance)
		{
			GameInstance->RemoveSplitScreenPlayers();
		}

		AShooterPlayerController* const PrimaryPC = Cast<AShooterPlayerController>(GetGameInstance()->GetFirstLocalPlayerController());
		if (PrimaryPC)
		{
			check(PrimaryPC->GetNetMode() == ENetMode::NM_Client);
			PrimaryPC->HandleReturnToMainMenu();
		}
	}

}
