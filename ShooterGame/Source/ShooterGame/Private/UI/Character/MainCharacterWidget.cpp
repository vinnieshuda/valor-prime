// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "MainCharacterWidget.h"

AShooterPlayerController* UMainCharacterWidget::GetPC()
{
	if (!CastedPC)
		CastedPC = Cast<AShooterPlayerController>(GetOwningPlayer());

	return CastedPC;
}

AShooterCharacter* UMainCharacterWidget::GetCharacter()
{
	if (!GetOwningPlayer())
		return nullptr;

	if (!GetOwningPlayerPawn())
		return nullptr;

	if (GetOwningPlayerPawn()->IsPendingKill())
		return nullptr;
	
	if (!CastedOwner)
		CastedOwner = Cast<AShooterCharacter>(GetOwningPlayerPawn());

	if (!CastedOwner)
		return nullptr;

	return CastedOwner;
}


