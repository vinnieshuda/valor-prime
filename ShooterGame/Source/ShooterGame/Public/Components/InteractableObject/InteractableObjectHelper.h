// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "InteractableObjectHelper.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UInteractableObjectHelper : public UBoxComponent
{
	GENERATED_BODY()

public:

	UInteractableObjectHelper();

	void SetRootComponentAttachment(USceneComponent* InParent, FName InSocketName = NAME_None);

	// Actor component interfaces

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	virtual void OnComponentCreated() override;
	virtual void PostInitProperties() override;
	virtual void BeginPlay() override;

	// Custom exposed Variables

	/* Collision channel for interactable objects, if you dont have add one in project config */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Actor Config | Collision")
	TEnumAsByte<ECollisionChannel> InteractableCollisionChannel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Actor Config | Widget Control")
	FName WidgetComponentTag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Actor Config | Widget Control")
	TSubclassOf<UWidgetComponent> WidgetComponentClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Actor Config | Widget Control")
	bool bUseWidgetComponent;

	// Custom Functions
	UWidgetComponent* GetControlledWidgetComponent();

private:

	UPROPERTY()
	UWidgetComponent* WidgetComponent;
	
};
