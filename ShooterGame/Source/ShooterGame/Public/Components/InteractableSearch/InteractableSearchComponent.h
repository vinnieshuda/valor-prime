// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "InteractableSearchComponent.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UInteractableSearchComponent : public USphereComponent
{
	GENERATED_BODY()
	
public:

	UInteractableSearchComponent();

	// ComponentInterface
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
/*	virtual void PostInitProperties() override;*/
	virtual void OnComponentCreated() override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	// Custom Functions
	UFUNCTION()
	void OnInteractableActorOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnInteractableActorOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//////////////////////////////////////////////////////////////////////////
	// Handle interaction
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnInteractPressed(AActor* InteractOwner, AActor* InteractedActor);

	UFUNCTION(BlueprintCallable, Category = "Interaction Component")
	void OnInteractPressed(AActor* InteractOwner);

	// Duplicated function to handle server checks
	void Internal_OnInteractPressed(AActor* InteractOwner, AActor* InteractedActor);

	//////////////////////////////////////////////////////////////////////////
	// Exposed Variables

	/* if this true interactable actor trace tests should run only when interactable actor is near aka collided with search sphere */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Defaults")
	bool bUseSphereSearch;

	/* if this true interactable trace will start from camera */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Defaults")
	bool bUseCameraForTrace;

	/* Collision channel for interactable objects, if you dont have add one in project config */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Collision")
	TEnumAsByte<ECollisionChannel> InteractableCollisionChannel;

	/* Collision channel for interactable objects, if you dont have add one in project config */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Collision")
	TArray<TEnumAsByte<ECollisionChannel>> BlockingChannels;

	/* Interactable Search range, this is not equal with MaxInteractableDistance!! This used for performance reason */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Search")
	float InteractableSearchRange;

	/* Max Interactable distance. How far can character interact with any object */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Search")
	float MaxInteractableDistance;

	/* Max Interactable distance. How far can character interact with any object */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Config | Debug")
	bool bDebugDraw;

private:

	//////////////////////////////////////////////////////////////////////////
	// Inner variables
	
	UPROPERTY()
	float ElapsedInteractTime;

	UPROPERTY()
	bool bInteractSearchStarted;

	UPROPERTY()
	AActor* PreviousHitActor;
	
};
