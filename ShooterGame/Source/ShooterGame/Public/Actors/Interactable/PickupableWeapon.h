// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Interactable/BaseInteractableActor.h"
#include "Types/InventoryTypes.h"
#include "Components/WidgetComponent.h"
#include "PickupableWeapon.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API APickupableWeapon : public ABaseInteractableActor
{
	GENERATED_BODY()
	
public:

	APickupableWeapon();

	UPROPERTY(EditDefaultsOnly, Category = "Default Config")
	UStaticMeshComponent* ItemMeshComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Default Config")
	UWidgetComponent* WidgetComponent;

	UPROPERTY(Replicated)
	FInventoryData ItemInventoryData;

	void SetupPickupableWeapon(int32 inCurrentAmmo, int32 inCurrentAmmoinClip, FInventoryData inItemInventoryData, UStaticMesh* inStaticMesh);

	UFUNCTION(Client, Reliable)
	void Client_SetupCollisionBox(FVector inBoxExtent);

	UPROPERTY()
	int32 CurrentAmmo;

	UPROPERTY()
	int32 CurrentAmmoInClip;

	virtual void OnInteractPressed_Implementation(AActor* InteractOwner);
	
};
