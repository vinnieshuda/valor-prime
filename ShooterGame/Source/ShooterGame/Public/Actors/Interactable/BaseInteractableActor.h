// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InteractableObject/InteractableObjectHelper.h"
#include "Interfaces/Interactable/InteractableInterface.h"
#include "Interfaces/Interactable/InteractableWidgetInterface.h"
#include "BaseInteractableActor.generated.h"

UCLASS()
class SHOOTERGAME_API ABaseInteractableActor : public AActor, public IInteractableInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseInteractableActor();

	USceneComponent* RootScene;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Actor")
	UInteractableObjectHelper* InteractableComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void OnInteractableEntered_Implementation();
	void OnInteractableLeaved_Implementation();
	void OnInteractableTargeted_Implementation(AActor* InteractOwner);
	void OnInteractableWasTargeted_Implementation();
	void OnInteractPressed_Implementation(AActor* InteractOwner);
	
};
