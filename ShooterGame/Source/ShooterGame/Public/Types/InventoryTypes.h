#pragma once

#include "InventoryTypes.generated.h"

class AShooterWeapon;

UENUM(BlueprintType)
enum class E_ItemType : uint8
{
	E_None										UMETA(DisplayName = "ItemType None"),
	E_Weapon									UMETA(DisplayName = "ItemType Weapon"),
	E_Magazine									UMETA(DisplayName = "ItemType Magazine"),
	E_CraftingMaterial							UMETA(DisplayName = "ItemType Crafting"),
	E_ConsumableEat								UMETA(DisplayName = "ItemType Eat"),
	E_ConsumableDrink							UMETA(DisplayName = "ItemType Drink"),
	E_Medical									UMETA(DisplayName = "ItemType Medical")
};

USTRUCT(BlueprintType)
struct FInventoryDatabase : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Database")
	UStaticMesh* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Database")
	E_ItemType ItemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Database")
	TSubclassOf<AShooterWeapon> WeaponClass;

};

USTRUCT(BlueprintType)
struct FInventoryData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Database")
	FDataTableRowHandle ItemRowHandler;

	FInventoryData()
	{
		ItemRowHandler.DataTable = nullptr;
		ItemRowHandler.RowName = NAME_None;
	}

	FInventoryData(FDataTableRowHandle inRowHandler)
	{
		ItemRowHandler = inRowHandler;
	}

};
