// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainCharacterWidget.generated.h"

class AShooterPlayerController;
class AShooterCharacter;

UCLASS()
class SHOOTERGAME_API UMainCharacterWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character Widget")
	class AShooterPlayerController* GetPC();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character Widget")
	class AShooterCharacter* GetCharacter();


private:
	
	class AShooterPlayerController* CastedPC;
	class AShooterCharacter* CastedOwner;
	
};
