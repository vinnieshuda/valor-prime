// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableWidgetInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UInteractableWidgetInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHOOTERGAME_API IInteractableWidgetInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Widget Interface")
	void OnInteractableActorEntered();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Widget Interface")
	void OnInteractableActorLeaved();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Widget Interface")
	void OnInteractableActorTargeted(AActor* InteractOwner);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Widget Interface")
	void OnInteractableActorNotTargeted();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Widget Interface")
	void OnInteractPressed();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Widget Interface")
	void OnInteractTimeUpdated(float inElapsedTime);
	
};
