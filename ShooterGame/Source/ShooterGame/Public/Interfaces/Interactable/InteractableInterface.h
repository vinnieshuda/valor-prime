// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UInteractableInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHOOTERGAME_API IInteractableInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Actor Interface")
	void OnInteractableEntered();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Actor Interface")
	void OnInteractableLeaved();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Actor Interface")
	void OnInteractableTargeted(AActor* InteractOwner);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Actor Interface")
	void OnInteractableWasTargeted();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interactable Actor Interface")
	void OnInteractPressed(AActor* InteractOwner);
	
};
